var gulp = require('gulp'),
    jade = require('gulp-jade'),
    connect = require('gulp-connect'),
    less = require('gulp-less'),
    coffee = require('gulp-coffee'),
    rjs = require('gulp-requirejs'),
    uglify = require('gulp-uglify'),
    clean = require('gulp-clean'),
    spritesmith = require('gulp.spritesmith');

gulp.task('jade', function() {
    gulp.src(['jade/*.jade','!jade/_*.jade'])
    .pipe(jade({
        pretty: "    "
        }))
    .pipe(gulp.dest('dist'))
    .pipe(connect.reload());
});

gulp.task('less', function() {
    gulp.src('less/app.less')
    .pipe(less())
    .pipe(gulp.dest('dist/css'))
    .pipe(connect.reload());
});

gulp.task('js', function() {
    gulp.src('dist/js/*.js')
    .pipe(connect.reload());
});

gulp.task('sprite', function() {
    var spriteData = 
        gulp.src('dist/images/about/*.png')
            .pipe(spritesmith({
                imgName: 'about-sprite.png',
                cssName: '_about-sprite.css',
                algorithm: 'top-down',
                padding: 4,
                sort: true
            }));

    spriteData.img.pipe(gulp.dest('dist/images/'));
    spriteData.css.pipe(gulp.dest('dist/css/'));
});

gulp.task('coffee', function() {
    gulp.src('coffee/*.coffee')
    .pipe(coffee())
    .pipe(gulp.dest('dist/js'))
    .pipe(connect.reload());
});

gulp.task('watch', function() {
    gulp.watch('jade/*.jade', ['jade']);
    gulp.watch('dist/js/*.js', ['js']);
    gulp.watch('less/*.less', ['less']);
    gulp.watch('coffee/*.coffee', ['coffee']);
});

gulp.task('connect', function() {
    connect.server({
        port: 1333,
        livereload: true,
        root: 'dist'
    });
});

gulp.task('default', ['jade', 'watch', 'connect', 'less', 'coffee']);