(function() {
    var $body = $("body"),
      $carousel = $(".carousel"),
      desktopWidth = 1024;

    var map, location = {lan: 51.508087, lng: -0.10669610000002194}, isNavOpen = false, isAnimated = false,
      image = new google.maps.MarkerImage('images/google-marker.png', null, null,
        new google.maps.Point(20, 20),
        new google.maps.Size(40, 40)
      );

    function initialize() {
        var options = {
            center: new google.maps.LatLng(location.lan, location.lng),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            draggable: false
        };
        map = new google.maps.Map(document.getElementById('google-map'), options);

        marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.lan, location.lng),
            map: map,
            title: 'Marker Title',
            icon: image
        });
    };

    google.maps.event.addDomListener(window, 'load', initialize);

    window.onload = function() {
        var openTarget = $('.nav-toggle'),
          closeTarget = $('.substrate'),
          navPanel = $('.navigation-panel'),
          mouseEnterPosition = 'un';

        fadelogo();
        initTopScroll();
        toogleClassForElement(openTarget, closeTarget, navPanel, 'navigation-panel-open');
        scrollTo();
        slideToClients();
        centralSeeTheWorkButton();
        mouseEnterImage(mouseEnterPosition);
        slideUserFavorites();
        scrollToTop();
        initSmallPopups();
        brandPopups();

        setInterval(function() {
            rightSlideFavorites();
        }, 3000);

        $('.close-navigation').click(function() {
            toogleClassForElement(openTarget, closeTarget, navPanel, 'navigation-panel-open');
        });

    };

    function isDesctop(){
        var mq = window.matchMedia('all and (min-width: ' + desktopWidth + 'px)');
        return mq.matches;
    }

    function initSmallPopups(){
        $(".addit-link:not(.primary-m)").magnificPopup({
            type: "inline",
            mainClass:"fs-popup-dark",
            callbacks: {
                open: function() {
                    var self = this;
                    $(".icon-modal-close").click(function(){
                        self.close();
                    });
                }
            }
        });
    }

    function brandPopups() {
        $(".section-default#clients").magnificPopup({
            type: 'inline',
            mainClass: "fs-popup",
            gallery:{
                enabled:true
            },
            delegate: '.primary-m',
            callbacks: {
                buildControls: function() {
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                },
                open: function() {
                    var self = this;
                    $(".icon-modal-close").click(function(){
                        self.close();
                    });
                }
            }
        });
    }

    //see the work on slider
    function centralSeeTheWorkButton() {
        var items = getItems($carousel);
        var mp = $.magnificPopup.instance;
        $('#see-the-work').magnificPopup({
            items: items,
            type: 'inline',
            gallery:{
                enabled:true
            },
            mainClass:"fs-popup",
            callbacks: {
                buildControls: function() {
                    this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
                },
                beforeOpen: function() {
                    mp.index = getIndex(items,$carousel.find(".cycle-slide-active .item-img").attr("data-mfp-src"));
                },
                open: function() {
                    var self = this;
                    $(".icon-modal-close").click(function(){
                        self.close();
                    });
                }
            }
        });

        function getItems($carousel) {
            var items = [];
            var $carouselItems = $carousel.find(".item-img");
            if ($carouselItems.length) {
                $carouselItems.each(function (indx, element) {
                    items.push({
                        src: $(element).attr("data-mfp-src")
                    });
                });
            }
            return items;
        }

        function getIndex(items, curentId){
            var indx = 0;
            items.forEach(function(item, index, arr){
                if(curentId === item.src){
                    indx = index;
                }
            });

            return indx;
        }
    }

    function fadelogo() {
        $doc = $(document),
          scrollTopi = $doc.scrollTop();
        elHeight = $('.main-content .section-full').innerHeight();
        $logoDark = $('.logo-dark'),
          $fixedLogoBlock = $('.wrap-fixed');

        if(scrollTopi > elHeight - 1) {
            $logoDark[0].style.zIndex = '1';
            $logoDark[0].style.opacity = '1';
        }
        if(scrollTopi > elHeight) {
            $fixedLogoBlock.css({'background-color': 'rgba(255, 255, 255, .9)'});
        }
        $logoDark.scroolly([
            {
                from: 'con-top',
                to: 'con-bottom = top',
                cssFrom:{
                    'opacity': '.0',
                    'z-index': '0'
                },
                cssTo:{
                    'opacity': '1',
                    'z-index': '1'
                }
            }
        ], $('.section-full'));

        $('.header .logo-light').scroolly([
            {
                from: 'con-top',
                to: 'con-bottom = top',
                cssFrom:{
                    'opacity': '1'
                },
                cssTo:{
                    'opacity': '0'
                }
            }
        ], $('.section-full'));
        $fixedLogoBlock.scroolly([
            {
                from: 'con-top',
                to: 'con-bottom = top',
                cssFrom:{
                    'background-color': 'rgba(255, 255, 255, .0)'
                },
                cssTo:{
                    'background-color': 'rgba(255, 255, 255, .9)'
                }
            }
        ], $('.section-full'));
    }

    function initTopScroll() {
        var TOP_SCROLL_LEVEL = 20,
          $doc = $(document),
          scrollTopi = $doc.scrollTop(),
          elHeight = $('.main-content .section-full').innerHeight();

        scrollTopi === 0 ? shouldBeAnim = true : shouldBeAnim = false;

        $(window).on('scroll.FTopScroll', function(e) {
            scrollToClientBlock($doc, elHeight, TOP_SCROLL_LEVEL);
        });

        $(window).on('resize', function() {
            elHeight = $('.main-content .section-full').innerHeight();
            fadelogo();
        });
    };

    function scrollToTop() {
        $('.logo-light, .logo, .logo-dark').click(function() {
            $('html, body').animate({ scrollTop: 0 }, 1000);
        });
    };

    //white button
    function slideToClients() {
        $('.slide-to-bottom').click(function(e) {
            var $doc = $(document),
              elHeight = $('.main-content .section-full').innerHeight();

            scrollToClientBlock($doc, elHeight);
        });
    };

    function scrollToClientBlock($doc, elHeight, TOP_SCROLL_LEVEL) {
        if(!isDesctop()){
            return false;
        }
        if(TOP_SCROLL_LEVEL){
            var scrollTopi = $doc.scrollTop();
            if (scrollTopi >= TOP_SCROLL_LEVEL && shouldBeAnim && !isAnimated) {
                shouldBeAnim = false;
                isAnimated = true;
                disableScroll();
                $('html, body').animate({scrollTop: elHeight}, 1000, function () {
                    isAnimated = false;
                    enableScroll();
                });
            }
            if (scrollTopi < 20) {
                shouldBeAnim = true;
            }
            else {
                shouldBeAnim = false;
            }
        } else if(!isNavOpen && !isAnimated) {
            isAnimated = true;
            disableScroll();
            $('html, body').animate({ scrollTop: elHeight }, 1000, function() {
                isAnimated = false;
                enableScroll();
            });
        }
    };


    function mouseEnterImage(mouseEnterPosition) {
        $('.tab-img').mouseenter(function(e) {
            if(mouseEnterPosition !== 'un')
                $('.comm-block-img').removeClass(mouseEnterPosition);
            var img = $(this)[0],
              className = img.className,
              iconClass = (className + ' ').slice(41, -1),
              iconHover = iconClass + '-2';
            mouseEnterPosition = iconHover;

            var deleteText = $('.content-for-communication .text');
            deleteText.removeClass('transition');

            var addText = $('#' + $(this)[0].id +'.text');
            addText.addClass('transition');
            $(this).addClass(iconHover);
        });
    };

    function rightSlideFavorites() {
        var visible = $('.about-text-visible');
        visible.each(function(indx) {
            if($(this).next().length > 0) {
                $(this).removeClass('about-text-visible');
                $(this).next().addClass('about-text-visible');
            } else {
                $(this).removeClass('about-text-visible');
                $(this).parent('.about').find('.about-text:first-child').addClass('about-text-visible');
            }
        });
    };

    function slideUserFavorites() {
        $('.about-left').click(function() {
            var wrap = $(this).parent('.about-wrap');
            var visible = wrap.find('.about-text-visible');
            var aboutText = wrap.find('.about-text');
            var reset = wrap.find('.about-text').length - 1;
            var prev = visible.prev();
            if (visible.prev().length > 0) {
                visible.removeClass('about-text-visible');
                prev.addClass('about-text-visible');
            } else {
                visible.removeClass('about-text-visible');
                $(aboutText).last().addClass('about-text-visible');
            }
        });

        $('.about-right').click(function() {
            var wrap = $(this).parent('.about-wrap');
            var visible = wrap.find('.about-text-visible');
            var aboutText = wrap.find('.about-text');
            var reset = wrap.find('.about-text').length - 1;
            var next = visible.next();
            if (visible.next().length > 0) {
                visible.removeClass('about-text-visible');
                next.addClass('about-text-visible');
            } else {
                visible.removeClass('about-text-visible');
                $(aboutText).first().addClass('about-text-visible');
            }
        });
    };

    function toogleClassForElement(openTarget, closeTarget, addClassEl, addClassName) {
        var openNav = false;
        var mainContent = $('.main-content');
        var deltaScrollTop;

        $(document).click(function(e) {
            var el = e;
            if(openNav === false) {
                open(el)
            } else {
                close(el);
            }
        });

        function open(e) {
            if(e.target.className.indexOf('nav-toggle') >= 0) {
                addClassEl.addClass(addClassName);
                closeTarget.addClass('visible');
                mainContent.addClass('blur');
                $carousel.addClass('blur');
                deltaScrollTop = $(document).scrollTop();
                //$('body').addClass('fixed-body');
                //$('body').addClass('fixed-body')
                openNav = true;
                isNavOpen = true;
            }
        }

        function close(e) {
            var tarClass = e.target.className;
            if(tarClass === closeTarget[0].className || tarClass === 'h1' || tarClass === 'h-auxiliary' || tarClass.indexOf('close-navigation') >= 0) {
                addClassEl.removeClass(addClassName);
                closeTarget.removeClass('visible');
                mainContent.removeClass('blur');
                $carousel.removeClass('blur');
                //$('body').removeClass('fixed-body');
                openNav = false;
                if(!(deltaScrollTop >= 0 && deltaScrollTop <= 20)){
                    shouldBeAnim = false;
                }
                setTimeout(function() {
                    isNavOpen = false;
                }, 2000);
            }
        }
    };

    function scrollTo() {
        $(document).click(function(e) {
            var element = $(e.target),
              link = '#';

            var openTarget = $('.nav-toggle'),
              closeTarget = $('.substrate'),
              navPanel = $('.navigation-panel');

            if (element[0].tagName === 'LI' && element.parent('.navigation') || element.parent('.navigation li.h1').length > 0 ) {
                e.target.tagName === 'LI' ? link += e.target.id : link += element.parent('.navigation li.h1')[0].id;
                var el = $(".main-content " + link);
                $('body, html').animate({ scrollTop: el.offset().top }, 1000);

                toogleClassForElement(openTarget, closeTarget, navPanel, 'navigation-panel-open');
            }
        });
    };

    /*function nextModal() {
     $('.next-modal').click(function(e) {
     var id = $('.themodal-overlay .modal')[0].id, modalsWrap;
     var newId = id.slice(-3, -1);
     if(newId >= 20) {
     modalsWrap = '.wrapper-see-the-modal';
     } else {
     modalsWrap = '.wrapper-primary-modal';
     }
     var allModal = $(modalsWrap + ' ' + '.modal');
     var lastEl = allModal.length -1;
     var modal = $(modalsWrap + ' ' + '#'+id).next();
     $.modal().close();
     if(modal.length > 0) {
     modal.modal().open();
     $('.themodal-overlay').addClass('transition');
     } else {
     // allModal[lastEl].modal.modal().open();
     // $('.themodal-overlay').addClass('transition');
     }
     });
     };*/

    /*function prevModal(modalsWrap) {
     $('.prev-modal').click(function(e) {
     var id = $('.themodal-overlay .modal')[0].id, modalsWrap;
     var newId = id.slice(-3, -1);
     if(newId >= 20) {
     modalsWrap = '.wrapper-see-the-modal';
     } else {
     modalsWrap = '.wrapper-primary-modal';
     }
     var allModal = $(modalsWrap + ' ' + '.modal');
     var lastEl = allModal.length -1;
     var modalW = $(modalsWrap + ' ' + '#'+id).prev();
     $.modal().close();
     if(modalW.length > 0) {
     modalW.modal().open();
     $('.themodal-overlay').addClass('transition');
     } else {
     // allModal[lastEl].modal().open();
     // $('.themodal-overlay').addClass('transition');
     }
     });
     };*/

    /*function closeModal() {
     $(document).click(function(e) {
     var tar = e.target.className;
     if (tar.indexOf('close') !== -1 || tar.indexOf('modal-wrapper') !== -1) {
     $('.themodal-overlay').removeClass('transition');
     setTimeout(function() {
     e.preventDefault();
     $.modal().close();
     }, 500);
     }
     });
     };*/


//disable nad enable scroll
    var keys = {37: 1, 38: 1, 39: 1, 40: 1};

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove  = preventDefault; // mobile
        document.onkeydown  = preventDefaultForScrollKeys;
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }

})();